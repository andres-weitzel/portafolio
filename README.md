# Portafolio_AndresWeitzel
Portafolio Personal Desarrollado con Bootstrap, Angular, 

## -----------------------------------------------------------------------

ClipBoard --> https://zeroesandones.medium.com/how-to-copy-text-to-clipboard-in-angular-e99c0feda501
ClipBoard API --> https://material.angular.io/cdk/clipboard/api

## -----------------------------------------------------------------------

imag backg https://carontestudio.com/blog/como-poner-una-imagen-de-fondo-en-html/

card hover https://codepen.io/designtorch2020/pen/zYNzxWP

3d ejemplos https://codepen.io/search/pens?q=3d
other 3d https://codepen.io/collection/EJFla

3d Parallax CSS ; https://codepen.io/jamesmellers/pen/QQpEMZ

76 CSS Cards : https://freefrontend.com/css-cards/

Libreria Animaciones css https://animate.style/

Agrandar card hover: https://codepen.io/ainalem/pen/QWGNzYm

Futurisc 3d hover me : https://codepen.io/jouanmarcel/pen/NLgVjm

css hover efect circle gif : https://codepen.io/Jeremboo/pen/WwbjvL

22 CSS Cards Hover :https://freefrontend.com/css-card-hover-effects/

68 CSS Text Animations ; https://freefrontend.com/css-text-animations/

lazy loading : https://medium.com/@rishanthakumar/angular-lazy-load-common-styles-specific-to-a-feature-module-c3f81c40daf1#:~:text=Lazy%20loading%20is%20one%20of,lazy%20loading%20in%20Angular%20documentation.


### Migración de CSS a SCSS
* npm i --save-dev schematics-scss-migrate
* ng g schematics-scss-migrate:scss-migrate


# --------------------------------------------


### Scroll Angular (Api intersection observer)
* https://giancarlobuomprisco.com/angular/intersection-observer-with-angular

### Full Page Scrolling Angular 
* https://remotestack.io/angular-full-page-scrolling-tutorial-example/


# --------------------------------------------


### Smmoth Scroll angular 
* https://www.youtube.com/watch?v=z9hwWIlyHb0

# ---------------------------------------------
 
 ### Scroll Animations with GSAP ScrollTrigger 
 *  https://www.youtube.com/watch?v=lOIp44zCGc0&t=382s
 *  https://greensock.com/docs/v3/Plugins/ScrollTrigger

* https://www.youtube.com/watch?v=bN71SaaU8jM (VER ESTE)



# ---------------------------------------------

### Animation on Scroll
* https://github.com/abhazelton/animate-on-scroll

# -------------------------------------------------

### libreria animation
* https://github.com/abhazelton/animate-on-scroll


* Doc oficial : https://greensock.com/docs/v3/Plugins/ScrollTrigger


# ------------------------------------------------

### Dialog Flow con IA 
* https://dialogflow.cloud.google.com/#/agent/newagent-sngr/integrations


### Implementación Dialog Flow
* https://medium.com/@renzo.reccio/angular-dialogflow-7910f25e289d


### Configuraciones 
* https://cloud.google.com/dialogflow/es/docs/integrations/dialogflow-messenger


### Messenger profile api
* https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/#profile_properties


# -----------------------------------------------


### Documentación Creación de Gráficos con Highchart en Angular 12
* Npm, config, detalles,  etc (RECOMIENDO) : https://hackthestuff.com/article/how-to-use-highcharts-in-angular-12
* Doc Oficial : https://api.highcharts.com/highcharts/
* Doc Oficial Instalación : https://www.highcharts.com/docs/getting-started/install-from-npm

### Doc Tipos de Graficos
* Nuevo sitio Tipos de Graf : https://www.highcharts.com/blog/chartchooser/
* Ejemplo Base 2d : https://stackblitz.com/edit/highcharts-angular-basic-line-ucnkbj?file=src%2Fapp%2Fapp.component.ts
* Modificación Gráfico 2d a 3d : https://www.eduforbetterment.com/3d-pie-and-donut-chart-in-angular-using-highchart/

### Desarrollo Graf Network 
* Ejemplo Gráfico Base : https://stackblitz.com/edit/highcharts-angular-networkgraph?file=src%2Fapp%2Fapp.component.ts
* Implementación al Gráfico Algoritmo Barnes-hut : https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/series-networkgraph/barnes-hut-approximation/
* Otros Gráficos  : https://github.com/highcharts/highcharts-angular#getting-started
* Doc Oficial network graph : https://www.highcharts.com/blog/tutorials/network-graph/

### Desarrollo Graf Area Chart
* Ejemplo Gráfico Base : https://www.tutorialspoint.com/angular_highcharts/angular_highcharts_area_spline.htm
* Combinación de Gráficos : https://codepen.io/pen


### Desarrollo Graf Columnas stacks 3d 
* Ejemplo Gráfico Base : https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/column-stacked

### Desarrollo Graf Sandey
* Ejemplo Graf Base : https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/sankey-diagram

* Doc tipo Grafico : https://www.highcharts.com/docs/chart-and-series-types/sankey-diagram

# -----------------------------------------------
### Angular Popup

* Doc e instalación : https://www.npmjs.com/package/ng-angular-popup



# -----------------------------------------------
### Angular Material

##### progress bar
* Doc e instalación : https://material.angular.io/guide/getting-started
* Ejemplo : https://material.angular.io/components/progress-bar/examples

##### ngx ui loader
* https://www.npmjs.com/package/ngx-ui-loader
* Implements loader: https://tdev.app/ngx-ui-loader
* spinner types: https://tdev.app/ngx-ui-loader/demo/spinners


##### Tooltip
* https://v6.material.angular.io/components/tooltip/overview


##### Dialog
* https://v6.material.angular.io/components/dialog/api



# -----------------------------------------------
### Angular-Boostrap (Interacciones/Animaciones de Eventos)
* https://ng-bootstrap.github.io/#/home


##### Tooltip y otros modulos(instalacion y config)
*  https://www.itsolutionstuff.com/post/how-to-use-bootstrap-tooltip-in-angularexample.html

* Para nuevas versiones, además de lo anterior agregar en la consola...
* ng add @angular/localize
* npm i @popperjs/core



# -----------------------------------------------
##### PRODUCT CARD UI HOVER FLOATING
* https://codepen.io/katywellington91/pen/PoGVzwZ

##### Cards Overloaping
* https://codepen.io/lonekorean/pen/QWyjaeg

#### cards filter degradacion
* https://codepen.io/steveeeie/pen/NVWMEM
