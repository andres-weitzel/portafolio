import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NavbarModule } from './components/navbar/navbar.module';
import { CardsComponent } from './components/cards/cards.component';
import { CardsModule } from './components/cards/cards.module';
import { PresentationComponent } from './components/presentation/presentation.component';
import { PresentationModule } from './components/presentation/presentation.module';
import { ProyectsComponent } from './components/proyects/proyects.component';
import { ProyectsModule } from './components/proyects/proyects.module';
import { FooterComponent } from './components/footer/footer.component';
import { FooterModule } from './components/footer/footer.module';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { AnalyticsModule } from './components/analytics/analytics.module';
import { HighchartsChartModule } from 'highcharts-angular';
import { NgToastModule } from 'ng-angular-popup';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorLoaderService } from './services/interceptors/interceptor-loader/interceptor-loader.service';
import { NgxUiLoaderConfig, NgxUiLoaderHttpModule, NgxUiLoaderModule, SPINNER } from 'ngx-ui-loader';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CardsModalsComponent } from './components/cards-modals/cards-modals/cards-modals.component';
import { CardsModalsModule } from './components/cards-modals/cards-modals/cards-modals.module';


//--CONFIG SPINNER--
const ngxUiLoaderConfig: NgxUiLoaderConfig = {

  fgsType: SPINNER.threeStrings, // foreground spinner type
  fgsSize: 100,
  //fgsColor: 'red',

};
//--FIN CONFIG SPINNER--


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CardsComponent,
    CardsModalsComponent,
    PresentationComponent,
    ProyectsComponent,
    FooterComponent,
    AnalyticsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NavbarModule,
    CardsModule,
    CardsModalsModule,
    PresentationModule,
    ProyectsModule,
    AnalyticsModule,
    FooterModule,
    HighchartsChartModule,
    NgToastModule,
    BrowserAnimationsModule,//PARA TODOS LOS MODULOS IMPORTAR
    MatProgressBarModule,
    MatTooltipModule,
    MatDialogModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderHttpModule.forRoot({ showForeground: true }),
    NgbModule




  ],

  providers: [
    {provide:HTTP_INTERCEPTORS, useClass: InterceptorLoaderService,multi:true}
  ],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
