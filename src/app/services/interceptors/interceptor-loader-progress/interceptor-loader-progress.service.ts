import { Injectable} from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderProgressService } from '../../loader-progress/loader-progress.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorLoaderProgressService implements HttpInterceptor {


  constructor(

    public loaderProgressService:LoaderProgressService

    ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


    this.loaderProgressService.isLoading.next(true);

    return next.handle(req).pipe(
      finalize(
        ()=>{
          this.loaderProgressService.isLoading.next(false);
        }
      )

    );


  }
}
