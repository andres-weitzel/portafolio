import { TestBed } from '@angular/core/testing';

import { InterceptorLoaderProgressService } from './interceptor-loader-progress.service';

describe('InterceptorLoaderProgressService', () => {
  let service: InterceptorLoaderProgressService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InterceptorLoaderProgressService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
