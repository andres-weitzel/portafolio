import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoaderProgressService {

  constructor() { }

  public isLoading:BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

}
