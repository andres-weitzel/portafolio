import { TestBed } from '@angular/core/testing';

import { LoaderProgressService } from './loader-progress.service';

describe('LoaderProgressService', () => {
  let service: LoaderProgressService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoaderProgressService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
