
import { AfterViewInit, Component } from '@angular/core';

import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';





gsap.registerPlugin(ScrollTrigger);



@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})


export class PresentationComponent implements AfterViewInit {

  constructor() { }


  //Imagenes
  imgCube = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/presentation/blueCube.png";

  imgRockLeft = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/presentation/b.png";

  imgRockRight = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/presentation/c.png";

  imgFlechaDown = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/icons/flechaAbajo.png";



  //Gifs
  gifInfoAreas = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/gifs/info.gif";

  gifProgramador = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/gifs/programmer02.gif";

  gifSocialMedia = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/gifs/social.gif";

  gifGithub = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/gifs/github.gif";

  gifGitlab = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/gifs/gitlab.gif";

  gifYoutube = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/gifs/youtubeLogo.gif";

  gifLinkedin = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/gifs/linkedinGif.gif";



  initScrollTrigger() {

    //=========== ALERT ANIMATION =======
    gsap.to('.alert', {
      opacity: 0,
      duration: 0.3,

      scrollTrigger: {
        trigger: '.container',
        scrub: true,
        start: '50% 50%',
        end: '40% 40%'
      },



    });
    //===========  FIN ALERT ANIMATION =======
    //===========  BLUE CUBE ANIMATION =======
    gsap.to('#blueCubeAnim', {

      rotation: 360,
      duration: 1,
      yPercent: -180,
      xPercent: -300,



      scrollTrigger: {
        trigger: '#blueCubeAnim',
        scrub: true,
        start: '50% 50%',
        end: '30% 20%',

      },


    });
    //=========== FIN BLUE CUBE ANIMATION =======



    //=========== ROCK LEFT ANIMATION =======
    gsap.to('#rockLeftAnim', {

      xPercent: 200,
      delay: 2,
      scrollTrigger: {
        trigger: '#rockLeftAnim',
        scrub: true,
        start: '40% 40%',
        end: '30% 20%',

      },
    });

    //=========== FIN ROCK LEFT ANIMATION =======



    //=========== ROCK RIGHT ANIMATION =======
    gsap.to('#rockRightAnim', {

      xPercent: -200,
      delay: 2,
      scrollTrigger: {
        trigger: '#rockRightAnim',
        scrub: true,
        start: '40% 40%',
        end: '30% 20%',

      },
    });

    //=========== FIN ROCK RIGHT ANIMATION =======

    //=========== CIRCULOS ANIMATION =======

    gsap.to('#circulos', {


      opacity: 0,
      duration: 0.3,



      scrollTrigger: {
        trigger: '#circulos',
        scrub: true,
        start: '40% 40%',
        end: '30% 20%',

      },


    });

    //=========== FIN CIRCULOS ANIMATION =======







  }

  ngAfterViewInit() {
    this.initScrollTrigger();
  }

}


