
import { Component, Injectable, OnInit } from '@angular/core';
import { LoaderProgressService } from 'src/app/services/loader-progress/loader-progress.service';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';





@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {

   AUDIO = new Audio('assets/audio/deepSpace02.mp3');

   //Iconos

   //main
   iconLogo ='assets/icons/portafolio.png';

   //Navbar
   iconList ='assets/icons/navbar/list.png';
   iconHome ='assets/icons/navbar/home.png';
   iconTecnologias ='assets/icons/navbar/tecnologias.png';
   iconAnalytics ='assets/icons/navbar/analytics02.png';
   iconProyects ='assets/icons/navbar/proyects.png';
   iconCurriculum ='assets/icons/navbar/curriculum.png';
   iconContacto ='assets/icons/navbar/user.png';
   iconConfig ='assets/icons/navbar/config.png';
   iconSoporte ='assets/icons/navbar/support.png';
   iconCookies ='assets/icons/navbar/politicy.png';
   iconAcercaDe ='assets/icons/navbar/idea.png';
   iconWsp ='assets/icons/navbar/wsp.png';
   iconCorreo ='assets/icons/navbar/message.png';

   //Sound
   iconSonido ='assets/icons/navbar/sound.png';
   iconSonidoPlay ='assets/icons/navbar/sound/play.png';
   iconSonidoPause ='assets/icons/navbar/sound/pause.png';
   iconSonidoStop ='assets/icons/navbar/sound/stop.png';






  constructor(
public loaderProgressService:LoaderProgressService,
private ngxService:NgxUiLoaderService,
public matDialog:MatDialog
  ) { }

  ngOnInit(): void {
    this.hideNavbar();
   }


  //======== SECCIONES =======

  toSection(seccion:string){
// spin loader
this.ngxService.start();

setTimeout(() => {

  document.getElementById(seccion).scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});

  this.ngxService.stop();

}, 500);


  }


//======== SOUND =======

  startAudio(){
    this.AUDIO.volume = 0.3;
    this.AUDIO.load();
    this.AUDIO.play();

  }

  pauseAudio(){
    this.AUDIO.pause();
  }

  stopAudio(){
    this.AUDIO.currentTime = 0;

  }


  hideNavbar(){

    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
      var currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        document.getElementById("navbar").style.top = "0";
      } else {
        document.getElementById("navbar").style.top = "-100px";
      }
      prevScrollpos = currentScrollPos;
    }
  }




}
