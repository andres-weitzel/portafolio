import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsModalsComponent } from './cards-modals.component';

describe('CardsModalsComponent', () => {
  let component: CardsModalsComponent;
  let fixture: ComponentFixture<CardsModalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardsModalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
