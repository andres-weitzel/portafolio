import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsModalsComponent } from './cards-modals.component';

const routes: Routes = [{ path: '', component: CardsModalsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsModalsRoutingModule { }
