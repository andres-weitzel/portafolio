import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardsModalsRoutingModule } from './cards-modals-routing.module';
import { CardsModalsComponent } from './cards-modals.component';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule
  ]
})
export class CardsModalsModule { }
