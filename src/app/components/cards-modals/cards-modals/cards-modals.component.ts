import { Component, OnInit } from '@angular/core';
import { CardsComponent } from '../../cards/cards.component';

@Component({
  selector: 'app-cards-modals',
  templateUrl: './cards-modals.component.html',
  styleUrls: ['./cards-modals.component.scss']
})
export class CardsModalsComponent implements OnInit {

  constructor() { }

//TITTLE
    //Gifs
    gifLoaderTittle = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/loaderTittle.gif";


  //FRONTEND
    //Gifs
    gifFrontendCard = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/tech03.gif";
  //Lenguajes FrontEnd
  imgHtml = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/html5.png";
  imgCss = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/css3.png";
  imgJs = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/js.png";
  imgTs = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/typescript.png";
  //Herramientas
  imgScss = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/scss.png";
  imgThymeleaf = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/thymeleaf.png";
  imgVsc = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/vsc.png";
  //Librerias
  imgJquery = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/jquery.png";
  imgAngularMat = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/angularMaterial.png";
  imgHighchart = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/highChart.png";
  imgReact = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/reactJs.png";
  //Frameworks
  imgAngular = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/angular.png";
  imgBootstrap = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/frontend/bootstrap.png";



  //BACKEND
  //Gifs
  gifBackendCard = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/back-unscreen.gif";
  gifBackend02Card = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/hud-loading-unscreen.gif";
  //Lenguajes
  imgJava = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/java.png";
  imgPython = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/python.png";
  //Librerias
  imgLog4j = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/log4j.png";
  imgLombok = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/lombok.png";
  imgSwagger = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/webservice/swagger.png";
  imgOpenApi = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/webservice/openapi.png";
  //Herramientas
  imgMaven= "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/maven.png";
  imgPostman= "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/postman.png";
  imgGit= "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/git.png";
  imgCmd= "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/cmd.png";
  imgEclipse= "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/eclipse.png";
  imgSts= "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/backend/sts.png";
  //Framework
  imgSpring
  ="https://raw.githubusercontent.com/andresWeitzel/PortafolioGraphics/master/cards/backend/springBoot.png";
  imgHibern="https://raw.githubusercontent.com/andresWeitzel/PortafolioGraphics/master/cards/backend/hibernate.png";
  imgJsf="https://raw.githubusercontent.com/andresWeitzel/PortafolioGraphics/master/cards/backend/jsf.png";



//DATABASE
//Gif
gifDatabaseCard = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/Uhy5-unscreen.gif";
//Lenguajes
imgPlsql = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/pl-sql.png";
imgSql = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/sql.png";
imgPlpgsql = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/pl-pgsql.png";
//Herramientas
imgXampp = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/xampp.png";
imgSqlDev = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/sqldeveloper.png";
imgDbeaver = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/dbeaver.png";
//sgdb sql
imgOracle = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/oracle.png";
imgMysql = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/mysql.png";
imgPostgres = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/postgres.png";
//sgdb nosql
imgMongo = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/database/mongo.png";
imgFirebase = "https://raw.githubusercontent.com/andresWeitzel/PortafolioGraphics/master/cards/database/firebase.png";

//DATASCIENCE
//Gif
gifDatascienceCard = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/datascience/data3-unscreen.gif";
//Lenguajes
imgMatlab = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/datascience/matlab.png";
//Librerias
imgNumpy = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/datascience/numpy.png";
imgMatplotlib = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/datascience/matplotlib.png";
imgPandas = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/datascience/pandas.png";
//Herramientas
imgJupyter = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/datascience/jupyter.png";

imgPycharm = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/datascience/pycharm.png";


//EMBEDDED SYSTEMS
//Gif
gifEmbeddedCard = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/emb-unscreen.gif";
//Lenguajes
imgCMasMas = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/c%2B%2B.svg";
imgMicroPython = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/micropython.png";
imgArduino = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/arduino.png";
//Hardware
imgEsp8266 = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/esp8266.png";
imgMicrochip = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/microchip.png";
imgRaspberry = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/raspberry.png";
//Herramientas
imgPicCompiler = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/piccompiler.png";
imgThonny = "https://github.com/andresWeitzel/PortafolioGraphics/raw/master/cards/embedded/thonny.png";


  ngOnInit(): void {
  }

}
