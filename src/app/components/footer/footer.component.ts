import { Component, OnInit } from '@angular/core';
import { Clipboard } from '@angular/cdk/clipboard';
import { NgToastService } from 'ng-angular-popup';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {


  constructor(
    private clipboard: Clipboard,
    private toast: NgToastService,
    private ngxService: NgxUiLoaderService

    ) { }

  ngOnInit(): void {
  }


//------------UTILS -----------


//loader
spinLoader(ms:number){
  this.ngxService.start();
  setTimeout(() => {
    this.ngxService.stop();
  }, ms);

}


  copiarLink(textToCopy: string) {
    this.clipboard.copy(textToCopy);
    this.toast.success({detail:"Operación Éxitosa",summary:'Se ha copiado el item Correctamente',duration:5000});
}

mostrarEmail(email:string){
  this.toast.info({detail:"Detalles",summary:email,duration:4000});

  setTimeout(() => {
    this.spinLoader(5000);
  }, 2000);

}




}
