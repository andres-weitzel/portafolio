import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import highcharts3D from 'highcharts/highcharts-3d';
highcharts3D(Highcharts);

import HC_networkgraph from 'highcharts/modules/networkgraph';
HC_networkgraph(Highcharts);


import HC_sankey from 'highcharts/modules/sankey';
HC_sankey(Highcharts);




@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {


  }

  //============= NETWORK CHART =============
  Highcharts01: typeof Highcharts = Highcharts;

  //innerWidthHg01 = (window.innerWidth)-800;

  chartOptions01: Highcharts.Options = {

    credits: {
      enabled: false
    },

    chart: {
      type: 'networkgraph',
      height: 620,
      //width : this.innerWidthHg01 ,
      width: 600,
      marginTop: 90,

      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
          [0, 'rgb(48, 48, 96)'],
          [1, 'rgb(0, 0, 0)']
        ]
      },
      borderColor: '#000000',
      borderWidth: 2,
      className: 'dark-container',
      plotBorderColor: '#CCCCCC',

    },
    title: {
      text: 'Tecnologías de Uso ',
      style: {
        color: '#FFFFFF'
      }
    },
    subtitle: {
      text: 'Agrupaciones por Área y Tipo',
      style: {
        color: '#FFFFFF'
      }

    },
    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            enabled: false
          }
        }
      }]
    },
    tooltip: {
      headerFormat: '<span style="font-size:20px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{rgb(223, 246, 255)};padding:0">Stock : </td>' +
        '<td style="padding:0"><b> {point.y: f} unidades</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      networkgraph: {
        keys: ['from', 'to'],
        layoutAlgorithm: {
          enableSimulation: true,
          friction: -0.9,
          linkLength: 70,
          integration: 'verlet',
          approximation: 'barnes-hut',

          gravitationalConstant: 15

        }
      }
    },

    series: [
      {

        marker: {
          radius: 10,
          symbol: 'radius'
        },

        type: 'networkgraph',

        dataLabels: {
          enabled: true,
          linkFormat: "",
          allowOverlap: false,
          style: {
            font: '9pt Trebuchet MS, Verdana, sans-serif',
            color: '#FFFFFF'
          },

        },
        data: [
          ['STACKS', 'FRONTEND'],
          ['FRONTEND', 'Lenguajes F'],
          ['Lenguajes F', 'Html 5'],
          ['Lenguajes F', 'Css 3'],
          ['Lenguajes F', 'Js 3'],
          ['FRONTEND', 'Herramientas F'],
          ['Herramientas F', 'SCSS 1.40'],
          ['Herramientas F', 'Thymeleaf 3'],
          ['Herramientas F', 'VSC'],
          ['FRONTEND', 'Librerías F'],
          ['Librerías F', 'Animate.css'],
          ['Librerías F', 'Gsap'],
          ['Librerías F', 'NGX cookie'],
          ['Librerías F', 'HighCharts'],
          ['FRONTEND', 'Frameworks F'],
          ['Frameworks F', 'Angular 12'],
          ['Frameworks F', 'Bootstrap 5'],

          ['STACKS', 'BACKEND'],
          ['BACKEND', 'Lenguajes B'],
          ['Lenguajes B', 'Java 8'],
          ['Lenguajes B', 'Python 3'],
          ['BACKEND', 'Herramientas B'],
          ['Herramientas B', ' Apache Maven 3.8'],
          ['Herramientas B', 'Postman 9'],
          ['Herramientas B', 'Swagger 2.0'],
          ['Herramientas B', 'Git'],
          ['Herramientas B', 'Eclipse, Netbeans, STS'],
          ['BACKEND', 'Librerías B'],
          ['Librerías B', 'Lombok 1.18'],
          ['Librerías B', 'OpenApi 3'],
          ['BACKEND', 'Frameworks B'],
          ['Frameworks B', 'Spring Boot 2.6'],
          ['Frameworks B', 'JPA Hibernate 6'],
          ['Frameworks B', 'JSF 2.3'],

          ['STACKS', 'DATABASE'],
          ['DATABASE', 'Lenguajes DB'],
          ['Lenguajes DB', 'SQL'],
          ['Lenguajes DB', 'PL/SQL'],
          ['Lenguajes DB', 'PLPG/SQL'],
          ['DATABASE', 'Herramientas DB'],
          ['Herramientas DB', 'XAMPP 12'],
          ['Herramientas DB', 'Sql Developer 21.4'],
          ['Herramientas DB', 'DBeaver 21.3'],
          ['Herramientas DB', 'CMD 2.6'],
          ['DATABASE', 'SGDB SQL'],
          ['SGDB SQL', 'Oracle 12'],
          ['SGDB SQL', 'PostgreSQL 14.1'],
          ['SGDB SQL', 'MySQL 8'],
          ['DATABASE', 'SGDB NOSQL'],
          ['SGDB NOSQL', 'Firebase 9.6'],
          ['SGDB NOSQL', 'MongoDB 5'],



        ],
        nodes: [

          {
            id: 'STACKS',
            color: "#1363DF",
            mass: 20,//A mayor masa, menos interacion con la gravedad,



          },

          {
            id: 'FRONTEND',
            color: "#42C1DB",
            mass: 20,

          },
          {
            id: 'Lenguajes F',
            color: "#47B5FF",
            mass: 15
          },
          {
            id: 'Librerías F',
            color: "#47B5FF",
            mass: 15
          },

          {
            id: 'Herramientas F',
            color: "#47B5FF",
            mass: 15
          },

          {
            id: 'Frameworks F',
            color: "#47B5FF",
            mass: 15
          },
          {
            id: 'BACKEND',
            color: "#42C1DB",
            mass: 20
          },
          {
            id: 'Lenguajes B',
            color: "#47B5FF",
            mass: 15
          },
          {
            id: 'Librerías B',
            color: "#47B5FF",
            mass: 15
          },
          {
            id: 'Frameworks B',
            color: "#47B5FF",
            mass: 15
          },
          {
            id: 'Herramientas B',
            color: "#47B5FF",
            mass: 15
          },
          {
            id: 'DATABASE',
            color: "#42C1DB",
            mass: 20
          }, {
            id: 'Lenguajes DB',
            color: "#47B5FF",
            mass: 15
          }, {
            id: 'Herramientas DB',
            color: "#47B5FF",
            mass: 15
          }, {
            id: 'SGDB SQL',
            color: "#47B5FF",
            mass: 15
          }, {
            id: 'SGDB NOSQL',
            color: "#47B5FF",
            mass: 15
          },

        ],


      }
    ]
  };






  //========== FIN NETWORK CHART ==============

  //====== AREA/PIE COMBINED CHART ============

  Highcharts02: typeof Highcharts = Highcharts;


  chartOptions02: Highcharts.Options = {
    credits: {
      enabled: false

    },

    chart: {
      type: 'areaspline',
      height: 300,
      width: 800,
      marginTop: 30,
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
          [0, 'rgb(48, 48, 96)'],
          [1, 'rgb(0, 0, 0)']
        ]
      },
      borderColor: '#000000',
      borderWidth: 1,
      className: 'dark-container',
      plotBorderColor: '#CCCCCC',

    },
    title: {
      text: 'Proyectos y Desarrollos ',
      style: {
        color: '#FFFFFF'
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -10,
      y: 10,
      floating: true,
      borderWidth: 1,
      itemStyle: {
        font: '9pt Trebuchet MS, Verdana, sans-serif',
        color: 'white'
      },


    },
    xAxis: {
      categories: ['Sitios Web', 'App Web', 'App Desktop', 'Api Rest', 'Microservicios', 'Bases de Datos'],
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: {
        text: 'Cantidad',
        style: {
          color: 'white',
          lineWidth: 0.5,

        }
      },
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:20px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">Cantidad : </td>' +
        '<td style="padding:0"><b> {point.y: f} proyectos</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      area: {
        fillOpacity: 0.5,

      },

    },

    series: [
      {
        name: 'Total de Proyectos x Secciones',
        type: undefined,
        data: [2, 4, 5, 7, 1, 8],
        color: 'rgb(36, 47, 200)'


      },

      {
        type: 'pie',
        name: 'Cantidad',



        data: [{
          name: 'Sitios Web',
          y: 2,
          color: 'rgb(155, 163, 235)',



        }, {
          name: 'App Web',
          y: 4,
          color: 'rgb(125, 123, 235)'
        }, {
          name: 'App Desktop',
          y: 5,
          color: 'rgb(80, 80, 200)'
        }, {
          name: 'Api Rest',
          y: 7,
          color: 'rgb(50, 65, 180)'
        }, {
          name: 'Microservicios',
          y: 1,
          color: 'rgb(185, 200, 235)'
        }, {
          name: 'DBS',
          y: 8,
          color: 'rgb(36, 47, 155)'
        }],
        center: [90, 50],

        size: 100,
        showInLegend: false,
        dataLabels: {
          enabled: true,

          style: {
            font: '9pt Trebuchet MS, Verdana, sans-serif',
            color: '#FFFFFF'
          },

        }
      }
    ]
  };

  //======= FIN AREA/PIE COMBINED CHART ==========


  //========= BAR CHART ================

  Highcharts03: typeof Highcharts = Highcharts;

  chartOptions03: Highcharts.Options = {
    credits: {
      enabled: false

    },

    chart: {
      type: 'column',
      height: 300,
      width: 300,
      marginBottom: 90,
      style: {
        color: '#FFFFFF'
      },
      options3d: {
        enabled: true,
        alpha: 15,
        beta: 15,
        depth: 50,
        viewDistance: 60
      },
      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
          [0, 'rgb(48, 48, 96)'],
          [1, 'rgb(0, 0, 0)']
        ]
      },
      borderColor: '#000000',
      borderWidth: 2,
      className: 'dark-container',
      plotBorderColor: '#CCCCCC',
    },
    title: {
      //text: 'Proyectos por Marco Temporal ',
      text: ''


    },
    legend: {
      /*
          layout: 'vertical',
          align: 'left',
          verticalAlign: 'top',
          x: 0,
          y: 0,
          floating: true,
          borderWidth: 1,
          */

      layout: 'horizontal',
      floating: true,
      margin: 4,
      itemStyle: {
        font: '9pt Trebuchet MS, Verdana, sans-serif',
        color: 'white'
      },

    },
    xAxis: {
      categories: ['2020', '2021', '2022'],
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: {
        text: 'Cantidad',
        style: {
          color: 'white'
        }
      },
      labels: {
        style: {
          color: 'white'
        }
      }



    },
    tooltip: {
      headerFormat: '<span style="font-size:20px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b> {point.y: f} </b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },


    series: [

      {
        name: 'Sitios Web',
        type: undefined,
        data: [2, 0, 0],
        color: 'rgb(180, 200, 255)',


      }, {
        name: 'App Desktop',
        type: undefined,
        data: [4, 0, 0],
        color: 'rgb(160, 180, 255)'

      }, {
        name: 'App Web',
        type: undefined,
        data: [1, 3, 4],
        color: 'rgb(140, 161, 255)'
      }, {
        name: 'Api Rest',
        type: undefined,
        data: [0, 3, 6],
        color: 'rgb(110, 131, 255)'

      }, {
        name: 'Microserv.',
        type: undefined,
        data: [0, 1, 1],
        color: 'rgb(70, 90, 255)'

      }, {
        name: 'DBS',
        type: undefined,
        data: [2, 4, 3],
        color: 'rgb(50, 50, 255)'

      }


    ]
  };

  //================== FIN BAR CHART ======================


  //============= SANKEY CHART ===============

  Highcharts04: typeof Highcharts = Highcharts;

  chartOptions04: Highcharts.Options = {
    credits: {
      enabled: false

    },

    chart: {
      type: 'sankey',
      height: 300,
      width: 490,
      style: {
        color: '#FFFFFF'
      },

      backgroundColor: {
        linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        stops: [
          [0, 'rgb(48, 48, 96)'],
          [1, 'rgb(0, 0, 0)']
        ]
      },
      borderColor: '#000000',
      borderWidth: 2,
      className: 'dark-container',
      plotBorderColor: '#CCCCCC',
    },
    title: {
      text: '',
    },
    subtitle: {
      text: 'Top 2022',
      style: {
        color: '#FFFFFF'
      }

    },
    legend: {
      /*
      layout: 'vertical',
      align: 'left',
      verticalAlign: 'top',
      x: 0,
      y: 0,
      floating: true,
      borderWidth: 1,
      */
      layout: 'horizontal',
      floating: true,
      borderWidth: 2,
    },
    plotOptions: {
      column: {
        color: '#FFFFFF'
      }
    },
    accessibility: {
      point: {
        valueDescriptionFormat: '{index}. {point.from} to {point.to}.'
      }

    },

    series: [{
      keys: ['from', 'to', 'weight',],
      type: undefined,
      colors: ['rgb(50, 50, 255)', "#1363DF", "#42C1DB", "#47B5FF"],


      data: [

        //========== Proyectos ===============
        ['Microservicios', 'Stack Back', 2.5],

        ['App Microelectr.', 'Stack Back', 2.5],
        ['App Microelectr.', 'Stack Front', 2.5],

        ['App ElectroThings', 'Stack Back', 2.5],
        ['App ElectroThings', 'Stack Front', 2.5],


        ['App MicroFrontEnd ', 'Stack Back', 2.5],
        ['App MicroFrontEnd ', 'Stack Front', 2.5],


        //========== FIN Proyectos ===============


        //========== Tecnologias Front ===============

        ['Stack Front', 'Frameworks Front', 2],
        ['Stack Front', 'Herramientas Front', 2],

        ['Frameworks Front', 'Angular', 1],
        ['Frameworks Front', 'Bootstrap', 1],


        ['Herramientas Front', 'Thymeleaf', 1],
        ['Herramientas Front', 'Highchart', 1],
        ['Herramientas Front', 'Html,css,scss,js', 1],


        //========== Fin Tecnologias Front ===============


        //========== Tecnologias Back===============
        ['Stack Back', 'Frameworks Back', 2],
        ['Stack Back', 'Herramientas Back', 2],


        ['Frameworks Back', 'Spring Framework', 1],


        ['Spring Framework', 'Spring Boot', 0.5],
        ['Spring Framework', 'Spring Cloud', 0.5],
        ['Spring Framework', 'Spring  Security', 0.5],
        ['Spring Framework', 'Spring  Data JPA', 0.5],
        ['Spring Framework', 'Spring MVC', 0.5],


        ['Herramientas Back', 'Open-Api v3', 1],
        ['Herramientas Back', 'Swagger-UI', 1],
        ['Herramientas Back', 'Lombok', 1],
        ['Herramientas Back', 'Log4J', 1],
        ['Herramientas Back', 'JWT', 1],
        ['Herramientas Back', 'Api-REST', 1],

        ['Api-REST', 'Bases de Datos', 1],


        //========== FIN Tecnologias Back===============


        //========== Bases de Datos ===============
        ['Bases de Datos', 'MySQL', 0.5],
        ['Bases de Datos', 'PostgreSQ', 0.5],
        ['Bases de Datos', 'Oracle', 0.5],
        ['Bases de Datos', 'MongoDB', 0.5],
        //========== Fin Bases de Datos ===============

      ]
    }]
  };

  //======== FIN SANKEY CHART  ========



}
