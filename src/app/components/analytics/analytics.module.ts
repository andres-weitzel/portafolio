import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HighchartsChartModule } from 'highcharts-angular';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserModule,
    HighchartsChartModule

  ]
})
export class AnalyticsModule { }
